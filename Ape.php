<?php
    require_once("Animal.php");

    class Ape extends Animal {

        public $yell = "Auooo";
        
        function yell() {
            return $this->yell;
        }
    }
?>