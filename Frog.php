<?php
    require_once("Animal.php");

    class Frog extends Animal {
        
        public $legs = 4;
        public $jump = "hop hop";
        
        public function jump() {
            return $this->jump;
        }
    }
?>