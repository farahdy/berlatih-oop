<?php
    require_once("Animal.php");
    require_once("Ape.php");
    require_once("Frog.php");

    $sheep = new Animal("shaun");

    echo "Name of Animal : ".$sheep->get_name()."<br>"; // "shaun"
    echo "Total legs are : $sheep->legs <br>"; // 2
    echo "Cold Blooded is : $sheep->cold_blooded <br><br>"; // false
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    // index.php
    $kodok = new Frog("buduk");
    echo "Name of Animal : ".$kodok->get_name()."<br>";
    echo "Total legs are : $kodok->legs <br>"; 
    echo "Cold Blooded is : $kodok->cold_blooded <br>";
    echo "Sound when Jump : ".$kodok->jump()."<br><br>" ; // "hop hop"
    
    $sungokong = new Ape("kera sakti");
    echo "Name of Animal : ".$sungokong->get_name()."<br>";
    echo "Total legs are : $sungokong->legs <br>"; 
    echo "Cold Blooded is : $sungokong->cold_blooded <br>";
    echo "Sound when Yell : ".$sungokong->yell()."<br><br>"; // "Auooo"

    
?>